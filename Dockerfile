FROM maven:latest
WORKDIR /tmp
COPY . ./
RUN mvn clean
RUN mvn install
EXPOSE 9090
ENTRYPOINT ["mvn","spring-boot:run"]

