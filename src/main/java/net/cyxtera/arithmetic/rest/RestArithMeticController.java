package net.cyxtera.arithmetic.rest;

import net.cyxtera.arithmetic.model.Operator;
import net.cyxtera.arithmetic.services.ArithmeticService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/arithmetic")
public class RestArithMeticController {

    private static final Logger LOG = LoggerFactory.getLogger(RestArithMeticController.class);

    private ArithmeticService arithmeticService;

    @Autowired
    public RestArithMeticController(ArithmeticService arithmeticService) {
        this.arithmeticService = arithmeticService;
    }

    @GetMapping(value = "/{SessionUser}/{Operator}", produces = "application/json")
    public Operator GetArithMetic(@PathVariable("SessionUser") String sessionUser, @PathVariable("Operator") String operator) {
        LOG.info("sessionuser:\t{}\tOperator:\t{}", sessionUser, operator);
        Operator rOperator = new Operator();
        switch (operator) {
            case "sum":
                rOperator = arithmeticService.sumOperator(sessionUser);
                break;
            case "sub":
                rOperator = arithmeticService.subOperator(sessionUser);
                break;
            case "mul":
                rOperator = arithmeticService.mulOperator(sessionUser);
                break;
            case "div":
                rOperator = arithmeticService.divOperator(sessionUser);
                break;
            case "pow":
                rOperator = arithmeticService.divOperator(sessionUser);
                break;
        }
        return rOperator;
    }
}
