package net.cyxtera.arithmetic.rest;

import net.cyxtera.arithmetic.model.Operator;
import net.cyxtera.arithmetic.services.OperatorService;
import org.slf4j.ILoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/operator")
public class RestOperatorController {

    private OperatorService operatorService;

    @Autowired
    public RestOperatorController(OperatorService operatorService) {
        this.operatorService = operatorService;
    }

    @GetMapping(produces = "application/json")
    public List<Operator> GetAllOperators() {
        return operatorService.listAll();
    }

    @GetMapping(value = "/{SessionUser}", produces = "application/json")
    public List<Operator> GetAllOperators(@PathVariable("SessionUser") String sessionUser) {
        return operatorService.getOperatorSessionUser(sessionUser);
    }

    @PostMapping(produces = "application/json")
    public Operator PostOperator(@Valid @RequestBody Operator operator) {
        operatorService.createOperator(operator);
        return operator;
    }
}
