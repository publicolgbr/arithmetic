package net.cyxtera.arithmetic.rest;

import net.cyxtera.arithmetic.model.SessionUser;
import net.cyxtera.arithmetic.services.SessionUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/session")
public class RestSessionController {
    private SessionUserService sessionUserService;

    @Autowired
    public RestSessionController(SessionUserService sessionUserService) {
        this.sessionUserService = sessionUserService;
    }

    @GetMapping(produces = "application/json")
    public List<SessionUser> GetAllSessionUser()
    {
        return sessionUserService.listAll();
    }

    @PostMapping(produces = "application/json")
    public SessionUser PostSession() {
        SessionUser sessionUser=new SessionUser();
        sessionUser.setUsername("user");

        sessionUserService.createSessionUser(sessionUser);
        return sessionUser;
    }
}
