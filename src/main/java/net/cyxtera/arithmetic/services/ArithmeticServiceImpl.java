package net.cyxtera.arithmetic.services;

import net.cyxtera.arithmetic.model.Operator;
import net.cyxtera.arithmetic.repositories.OperatorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class ArithmeticServiceImpl implements ArithmeticService {

    private static final Logger LOG = LoggerFactory.getLogger(ArithmeticServiceImpl.class);

    private OperatorRepository operatorRepository;

    @Autowired
    public ArithmeticServiceImpl(OperatorRepository operatorRepository) {
        this.operatorRepository = operatorRepository;
    }

    @Override
    public Operator sumOperator(String sessionuser) {
        AtomicLong lResult = new AtomicLong();
        if (operatorRepository.findBySessionuser(sessionuser).size() > 0) {
            Operator operator = operatorRepository.findBySessionuser(sessionuser).get(0);
            lResult.set((long) operator.getOperatornumber());
            operatorRepository.delete(operator);
        }
        operatorRepository.findBySessionuser(sessionuser).forEach(o -> {
            lResult.set(lResult.get() + (long) o.getOperatornumber());
            operatorRepository.delete(o);
        });
        return OperatorRetorno(lResult.get(), sessionuser);
    }

    @Override
    public Operator subOperator(String sessionuser) {
        AtomicLong lResult = new AtomicLong();
        if (operatorRepository.findBySessionuser(sessionuser).size() > 0) {
            Operator operator = operatorRepository.findBySessionuser(sessionuser).get(0);
            lResult.set((long) operator.getOperatornumber());
            operatorRepository.delete(operator);
        }
        operatorRepository.findBySessionuser(sessionuser).forEach(o -> {
            lResult.set(lResult.get() - (long) o.getOperatornumber());
            operatorRepository.delete(o);
        });
        return OperatorRetorno(lResult.get(), sessionuser);
    }

    @Override
    public Operator mulOperator(String sessionuser) {
        AtomicLong lResult = new AtomicLong();
        if (operatorRepository.findBySessionuser(sessionuser).size() > 0) {
            Operator operator = operatorRepository.findBySessionuser(sessionuser).get(0);
            lResult.set((long) operator.getOperatornumber());
            operatorRepository.delete(operator);
        }
        operatorRepository.findBySessionuser(sessionuser).forEach(o -> {
            lResult.set(lResult.get() * (long) o.getOperatornumber());
            operatorRepository.delete(o);
        });
        return OperatorRetorno(lResult.get(), sessionuser);
    }

    @Override
    public Operator divOperator(String sessionuser) {
        AtomicReference<Double> dResult = new AtomicReference<>((double) 0);
        if (operatorRepository.findBySessionuser(sessionuser).size() > 0) {
            Operator operator = operatorRepository.findBySessionuser(sessionuser).get(0);
            dResult.set(operator.getOperatornumber());
            operatorRepository.delete(operator);
        }
        operatorRepository.findBySessionuser(sessionuser).forEach(o -> {
            dResult.set(dResult.get() / o.getOperatornumber());
            operatorRepository.delete(o);
        });
        return OperatorRetorno(dResult.get(), sessionuser);
    }

    @Override
    public Operator powOperator(String sessionuser) {
        AtomicReference<Double> dResult = new AtomicReference<>((double) 0);
        if (operatorRepository.findBySessionuser(sessionuser).size() > 0) {
            Operator operator = operatorRepository.findBySessionuser(sessionuser).get(0);
            dResult.set(operator.getOperatornumber());
            operatorRepository.delete(operator);
        }
        operatorRepository.findBySessionuser(sessionuser).forEach(o -> {
            dResult.set(Math.pow((double) dResult.get(), o.getOperatornumber()));
            operatorRepository.delete(o);
        });
        return OperatorRetorno(dResult.get(), sessionuser);
    }

    private Operator OperatorRetorno(double dResult, String sessionuser) {
        Operator operator = new Operator();
        operator.setOperatornumber(dResult);
        operator.setSessionuser(sessionuser);
        operatorRepository.save(operator);
        return operator;
    }
}
