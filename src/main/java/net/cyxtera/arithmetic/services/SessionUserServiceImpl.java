package net.cyxtera.arithmetic.services;

import net.cyxtera.arithmetic.model.SessionUser;
import net.cyxtera.arithmetic.repositories.SessionUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SessionUserServiceImpl implements SessionUserService {

    private SessionUserRepository sessionUserRepository;

    @Autowired
    public SessionUserServiceImpl(SessionUserRepository sessionUserRepository) {
        this.sessionUserRepository = sessionUserRepository;
    }

    @Override
    public SessionUser createSessionUser(SessionUser sessionUser) {
        sessionUserRepository.save(sessionUser);
        return sessionUser;
    }

    @Override
    public List<SessionUser> listAll() {
        List<SessionUser> sessionUsers=new ArrayList<>();
        sessionUserRepository.findAll().forEach(sessionUsers::add);
        return sessionUsers;
    }
}
