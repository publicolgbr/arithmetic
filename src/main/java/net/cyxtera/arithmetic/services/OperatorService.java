package net.cyxtera.arithmetic.services;

import net.cyxtera.arithmetic.model.Operator;

import java.util.List;

public interface OperatorService {
    Operator createOperator(Operator operator);

    List<Operator> listAll();

    List<Operator> getOperatorSessionUser(String idsessionuser);
}
