package net.cyxtera.arithmetic.services;

import net.cyxtera.arithmetic.model.Operator;
import net.cyxtera.arithmetic.model.SessionUser;

public interface ArithmeticService {
    Operator sumOperator(String sessionuser);
    Operator subOperator(String sessionuser);
    Operator mulOperator(String sessionuser);
    Operator divOperator(String sessionuser);
    Operator powOperator(String sessionuser);
}
