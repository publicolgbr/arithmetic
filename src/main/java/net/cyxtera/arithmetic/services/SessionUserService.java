package net.cyxtera.arithmetic.services;

import net.cyxtera.arithmetic.model.SessionUser;

import java.util.List;

public interface SessionUserService {

    SessionUser createSessionUser(SessionUser sessionUser);

    List<SessionUser> listAll();
}
