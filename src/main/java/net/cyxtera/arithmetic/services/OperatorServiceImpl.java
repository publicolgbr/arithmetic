package net.cyxtera.arithmetic.services;

import net.cyxtera.arithmetic.model.Operator;
import net.cyxtera.arithmetic.repositories.OperatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OperatorServiceImpl implements OperatorService {

    private OperatorRepository operatorRepository;

    @Autowired
    public OperatorServiceImpl(OperatorRepository operatorRepository) {
        this.operatorRepository = operatorRepository;
    }

    @Override
    public Operator createOperator(Operator operator) {
        operatorRepository.save(operator);
        return operator;
    }

    @Override
    public List<Operator> listAll() {
        List<Operator> operators = new ArrayList<>();
        operatorRepository.findAll().forEach(operators::add);
        return operators;
    }

    @Override
    public List<Operator> getOperatorSessionUser(String sessionuser) {
        List<Operator> operators = new ArrayList<>();
        operatorRepository.findBySessionuser(sessionuser).forEach(operators::add);
        return operators;
    }
}
