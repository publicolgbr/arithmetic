package net.cyxtera.arithmetic.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.util.Calendar;
import java.sql.Date;

@RedisHash("sessionuser")
public class Operator {
    @Id
    private String id;
    @Indexed
    private String sessionuser;
    private double operatornumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSessionuser() {
        return sessionuser;
    }

    public void setSessionuser(String sessionuser) {
        this.sessionuser = sessionuser;
    }

    public double getOperatornumber() {
        return operatornumber;
    }

    public void setOperatornumber(double operatornumber) {
        this.operatornumber = operatornumber;
    }
}
