package net.cyxtera.arithmetic.repositories;

import net.cyxtera.arithmetic.model.Operator;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OperatorRepository extends CrudRepository<Operator, String> {
    List<Operator> findBySessionuser(String sessionuser);
}
