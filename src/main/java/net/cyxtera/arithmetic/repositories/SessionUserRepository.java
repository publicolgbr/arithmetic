package net.cyxtera.arithmetic.repositories;

import net.cyxtera.arithmetic.model.SessionUser;
import org.springframework.data.repository.CrudRepository;

public interface SessionUserRepository extends CrudRepository<SessionUser,String> {
}
