package net.cyxtera.arithmetic;

import ai.grakn.redismock.RedisServer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ArithmeticApplicationTests {

	private static final Logger LOG = LoggerFactory.getLogger(ArithmeticApplicationTests.class);

    @Test
    public void contextLoads() throws IOException {
    	/*
        RedisServer server = RedisServer.newRedisServer();
        server.start();
        //LOG.info(server.getHost());
        assertTrue(server.getHost().equals("0.0.0.0"));*/
    }

}
